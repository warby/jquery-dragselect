##jQuery DragSelect 
`v0.0.1`

This plugin enables drag-to-select on an element.  The user clicks and holds, and then drags to create a selection.


###Plugin dependencies

* jQuery >= 1.5

###Plugin installation and basic usage

1. Download and extract the files `jquery.dragselect.js` and `jquery.dragselect.css` from `jquery-dragselect.zip` to wherever you like.
2. Add a script tag for the JS file and a link tag for the CSS file to your page:

	`<script type="text/javascript" 				src="/path/to/jquery.dragselect.js"> </script>`

	`<link rel="stylesheet" type="text/css" href="/path/to/jquery.dragselect.css" />`

3. Instantiate the plugin on an element in any of the following 3 ways:

		$('#some-element').dragselect(); // Calls init

		$('#some-element').dragselect({ 
			some_option: some_value
		}); // Calls init and passes options object

		$('#some-element').dragselect('init'); // Calls init


### Plugin options

	{
		/**
   		 * Elements to select when dragging
		 */
		selector: "> *",

		/**
   		 * Class to add to selected elements
		 */
		selectedElementsClass: "dragselect-selected",

		/**
   		 * Class to apply to the selection box
		 */
		selectionClass: "dragselect-selection",

		/**
   		 * Function to call when the set of selected elements changes. 
		 * Gets passed the array of selected elements.	
		 */
		onSelectionChanged: function(elements) {},

		/**
   		 * Function to call when dragging is stopped, either by the user 
		 * releasing the mouse or dragging outside the bounds of the element. 
		 * Gets passed the array of selected elements.	
		 */
		onStop: function(elements) {}
	}

### Plugin methods

To call specific methods, pass a string as an argument to the plugin function.  For example:

		$('#some-element').dragselect('init');

#### init

Initialises the plugin on this element.  Sets up event listeners to handle dragging behaviour.  Called if no parameters are supplied when calling `dragselect`, and when an object is passed.

### Roadmap

* Code testing and cross-browser testing
* Add option to only select elements wholly enclosed by the selection box
* Code cleanup

### License

As set out in LICENSE.txt, which should be included in the ZIP file.