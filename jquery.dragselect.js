/**
 * jQuery DragSelect (v0.0.1)
 *
 * A plugin which enables drag-to-select on an element.
 * Plugin homepage: https://bitbucket.org/james_duncan/jquery-dragselect
 *
 * @author James Warwood (james.duncan.1991@googlemail.com)
 */
(function ($) {

    /**
     * Main method, routes to other methods of plugin or errors if unknown method.
     *
     * @param method {Mixed} (optional) - Nothing - if not passed, init called
     *                                    Object - a list of options; if is passed, init called with object
     *                                    String - calls the method indicated, or errors
     */
    $.fn.dragselect = function(method) {

        if (typeof methods[method] === 'function') {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object') {
            method = $.extend({}, user_options, method);
            return methods.init.apply(this, [method]);
        } else if(!method) {
            return methods.init.apply(this, [user_options]);
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.dragselect' );
        }
    };

    var methods = {

        /**
         * Initialises the plugin and binds to appropriate mouse events.
         */
         init: function(options) {
            var $this = this;

            // Add class to element and append the selection div
            this.addClass('dragselect');
            this.append(properties.$selectionDiv.addClass(options.selectionClass));
            this.css({ position: 'relative' });

            var parentOffset = null;
            var targets = [];

            // Mousedown - set isDragging flag and initial mouse position
            this.bind('mousedown', function (ev) {

                // Elements to check collisions with
                targets = [];

                // Update dimensions of this element
                parentOffset = properties.$selectionDiv.parent().offset();

                properties.isDragging = true;

                // Set location properties
                properties.startLocation = {
                    x: ev.pageX - parentOffset.left,
                    y: ev.pageY - parentOffset.top
                };
                properties.currentLocation = $.extend({}, properties.startLocation);

                // Calculate bounding boxes for all immediate descendants of element
                var children = $(this).find(options.selector).not(properties.$selectionDiv);
                for (var c = 0; c < children.length; c++) {

                    $(children[c]).removeClass(options.selectedElementClass);

                    var obj = (function(child) {
                        return {
                            element: child,
                            bounding_box: private_methods.calculateBoundingBox(child)
                        };
                    })(children[c]);

                    targets.push(obj);
                }
            });

            // Bind to the mousemove to track drag
            this.bind('mousemove', function (ev) {
                if (!properties.isDragging) {
                    return;
                }

                // Show selection box
                properties.$selectionDiv.removeClass('hidden');

                // Update current pointer location
                properties.currentLocation.x = ev.pageX - $this.offset().left - properties.startLocation.x;
                properties.currentLocation.y = ev.pageY - $this.offset().top - properties.startLocation.y;

                // Selection box dimensions
                var newDimensions = {
                    left: properties.startLocation.x,
                    width: properties.currentLocation.x,
                    top: properties.startLocation.y,
                    height: properties.currentLocation.y
                };

                // Check if dragging backwards on x-axis
                if (properties.startLocation.x > ev.pageX - parentOffset.left) {
                    newDimensions.left = ev.pageX - parentOffset.left; // Left to current mouse position
                    newDimensions.width = properties.startLocation.x - newDimensions.left; // Width to difference between start location and new left
                }

                // Check if dragging backwards on x-axis
                if (properties.startLocation.y > ev.pageY - parentOffset.top) {
                    newDimensions.top = ev.pageY - parentOffset.top;
                    newDimensions.height = properties.startLocation.y - newDimensions.top;
                }

                // Update selection div css
                properties.$selectionDiv.css({
                    'left': newDimensions.left+"px",
                    'top': newDimensions.top+"px",
                    'width': newDimensions.width+"px",
                    'height': newDimensions.height+"px"
                });

                // Calculate bounding box for selection div
                var selectionBB = {
                    'x': newDimensions.left,
                    'y': newDimensions.top,
                    'w': newDimensions.width,
                    'h': newDimensions.height
                };

                // Track whether selection changes on this iteration
                var selectionAltered = false;

                // Loop through targets and check if colliding with selection box
                for (var bb = 0; bb < targets.length; bb++) {

                    if (private_methods.isColliding(selectionBB, targets[bb].bounding_box)) {
                        $(targets[bb].element).addClass(options.selectedElementClass);
                        selectionAltered = true;
                    } else {
                        $(targets[bb].element).removeClass(options.selectedElementClass);
                        selectionAltered = true;
                    }
                }

                // Fire selected changed function if appropriate
                if (selectionAltered && typeof options.onSelectionUpdate === 'function') {
                    options.onSelectionUpdate($(targets).filter(options.selectedElementClass));
                }
            });

            // Bind to mouseup - unset isDragging flag
            this.bind('mouseup mouseleave', function (ev) {
                properties.isDragging = false; // Unset flag
                properties.$selectionDiv.addClass('hidden'); // Hide selection box

                if (typeof options.stop === 'function') {
                    options.onStop($(targets).filter(options.selectedElementClass));
                }
            });
         }
    };

    /**
     * Private properties for plugin
     */
    var properties = {

        // Indicates whether user is currently dragging a selection or not
        isDragging: false,

        // Stores the start coordinates of the drag
        startLocation: null,

        // Current pointer location
        currentLocation: null,

        $selectionDiv: $('<div />', {
            "class": "hidden"
        })
    };

   /**
    * Private methods for plugin
    */
    var private_methods = {

        /**
         * Calculates the bounding box size and position for this element.
         *
         * @param element {jQuery object} - if not supplied, will calculate the bounding box of the current selection
         */
        calculateBoundingBox: function(element) {
            element = element ? $(element) : properties.$selectionDiv;
            var parentOffset = properties.$selectionDiv.parent().offset();

            var bb = {
                x: element.offset().left - parentOffset.left,
                y: element.offset().top - parentOffset.top,
                w: element.width(),
                h: element.height()
            };

            return bb;
        },

        isColliding: function(bb1, bb2) {
             return (bb1.x+bb1.w > bb2.x && bb2.x+bb2.w > bb1.x &&
                bb1.y+bb1.h > bb2.y && bb2.y+bb2.h > bb1.y);
        }
    };

    /**
     * Options which can be overriden by the user
     */
    var user_options = {
        selector: " > *",
        selectedElementClass: "dragselect-selected",
        selectionClass: "dragselect-selection"
    };

})(jQuery);